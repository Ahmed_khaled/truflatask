import { Comment } from '../../comments/entities/comment.entity';
import { User } from '../../users/entities/user.entity';
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn,
    OneToMany
} from 'typeorm';


@Entity()
export class Article {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255 })
    title: string;

    @Column({ type: 'text' })
    body: string;

    @Column({ default: 0 })
    thumps_up: number;

    @ManyToOne(() => User, user => user.articles)
    @JoinColumn({ name: "user" })
    user: User;

    // @ApiProperty({ type: () => [Comment] })
    @OneToMany(() => Comment, comment => comment.article)
    comments: Comment[];

    @CreateDateColumn({ name: 'created_at' })
    createdAt!: Date;

    @UpdateDateColumn({ name: 'updated_at' })
    updatedDate!: Date;
}
