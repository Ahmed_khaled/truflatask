import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { IsNotEmpty } from "class-validator";

export class CreateUserDto {

    @ApiProperty()
    @IsNotEmpty({ message: 'please enter your name' })
    name: string;

    @ApiProperty()
    @IsNotEmpty({ message: 'please enter your job title' })
    job_title: string;
}
