import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Crud, CrudController } from '@nestjsx/crud';
import { CommentsService } from './comments.service';
import { CreateCommentDto } from './dto/create-comment.dto';
import { Comment } from './entities/comment.entity';

@ApiTags('comments')
@Crud({
  model: {
    type: Comment
  },
  routes: {
    only: ["createOneBase", "getManyBase"],
  },
  dto: {
    create: CreateCommentDto
  }
})
@Controller('comments')
export class CommentsController implements CrudController<Comment> {
  constructor(public service: CommentsService) { }
}
