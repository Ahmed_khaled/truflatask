import { Inject, Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Comment } from './entities/comment.entity';

@Injectable()
export class CommentsService extends TypeOrmCrudService<Comment>{
  constructor(@Inject('COMMENT_REPOSITORY') repo) {
    super(repo)
  }
}
