# Node.js Backend challenge
 

The task is to create a backend API for a simple blog system consisting of Articles, Users and Comments.

## Features:
#### Articles
  - Create a new article ( title, body, and author ). 
  - Read/retrieve a given Article by ID. 
  - List all articles. 
  - Search for a specific article. 
  - Add comments to a given article. 
  - Thumbs up to a given article. 
  
  
#### Author
  - :heavy_check_mark: Create a new author ( name and job title ) 
  - :heavy_check_mark: Read/retrieve given Author info by ID. 
  - :heavy_check_mark: List all authors.  
	
	
## Requirements:
  - :heavy_check_mark: The features above need to be implemented in the form of a Restful API. 
  - :heavy_check_mark: The API needs to be implemented using Node.js 
  - :heavy_check_mark: Use MySQL as a Database to store the data. 
  - :heavy_check_mark: Use Expressjs as the HTTP server for your API. 
  - :heavy_check_mark: Implement input validation for all the endpoints.
  - Write unit and end to end test cases. 
  - :heavy_check_mark: Write design documentation for both the API and the data model visit `/swagger`.
  - The code should be hosted on one of the following source management systems ( Github, Gitlab ) 

## Bonus:
  - :heavy_check_mark: Implement the task using one of the frameworks that support Typescript, e.g. NestJs ( https://nestjs.com/ ) 
  - Use Docker and docker-compose to automate building and provisioning of the API and the database. 
  - :heavy_check_mark: Add functionality to sort articles by the number of thumbs up given. 
  - Enhance articles searching ( i.e. text scoring, search engines )
  - Deploy your API to one of the free hosting services, e.g. Heroku.

