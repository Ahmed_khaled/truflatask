import { ApiProperty } from '@nestjs/swagger';
import { Article } from '../../articles/entities/article.entity';
import { Comment } from '../../comments/entities/comment.entity';
import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToMany } from 'typeorm';

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty()
    @Column({ length: 255 })
    name: string;

    @ApiProperty()
    @Column({ length: 255 })
    job_title: string;

    @OneToMany(() => Article, article => article.user)
    articles: Article[];

    @OneToMany(() => Comment, comment => comment.user)
    comments: Comment[];

    @CreateDateColumn({ name: 'created_at' })
    createdAt!: Date;

    @UpdateDateColumn({ name: 'updated_at' })
    updatedDate!: Date;
}
