import { Controller, Get, UseInterceptors } from '@nestjs/common';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import { Crud, CrudController, CrudRequest, CrudRequestInterceptor, ParsedRequest } from '@nestjsx/crud';
import { ArticlesService } from './articles.service';
import { CreateArticleDto } from './dto/create-article.dto';
import { Article } from './entities/article.entity';

@ApiTags('articles')
@Crud({
  model: {
    type: Article
  },
  routes: {
    only: ["createOneBase", "getOneBase", "getManyBase"],
  },
  dto: {
    create: CreateArticleDto
  }
})
@Controller('articles')
export class ArticlesController implements CrudController<Article>{
  constructor(public service: ArticlesService) { }

  @UseInterceptors(CrudRequestInterceptor)
  @ApiParam({
    name: 'id',
    description: 'The id of the article',
    type: 'number',
  })
  @Get('/:id/thumps_up')
  async thumpsUp(@ParsedRequest() req: CrudRequest) {
    let article = await this.service.incrementThumpsUp(req);
    return article;
  }
}
