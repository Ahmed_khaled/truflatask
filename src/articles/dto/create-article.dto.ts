import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { IsNotEmpty } from "class-validator";
import { User } from "../../users/entities/user.entity";

export class CreateArticleDto {

    @ApiProperty()
    @IsNotEmpty({
        message: "please enter title"
    })
    title: string;

    @ApiProperty()
    @IsNotEmpty({
        message: "please enter body"
    })
    body: string;

    @ApiProperty({ type: 'number' })
    @IsNotEmpty({
        message: "please enter user id"
    })
    user: User;
}
