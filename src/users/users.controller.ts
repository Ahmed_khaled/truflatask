import { Controller } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UsersService } from './users.service';
import { Crud, CrudController } from '@nestjsx/crud';
import { User } from './entities/user.entity';
import { CreateUserDto } from './dto/create-user.dto';

@ApiTags('users')
@Crud({
  model: {
    type: User
  },
  routes: {
    only: ["createOneBase", "getOneBase", "getManyBase"],
  },
  dto: {
    create: CreateUserDto
  }
})
@Controller('users')
export class UsersController implements CrudController<User> {
  constructor(public service: UsersService) { }
}
