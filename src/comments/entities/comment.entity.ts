import { CrudValidationGroups } from "@nestjsx/crud";
import { Article } from "../../articles/entities/article.entity";
import { User } from "../../users/entities/user.entity";
import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

const { CREATE, UPDATE } = CrudValidationGroups;

@Entity()
export class Comment {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'text' })
    content: string;

    @ManyToOne(() => User, user => user.comments)
    @JoinColumn({ name: "user" })
    user: User;


    @ManyToOne(() => Article, article => article.comments)
    @JoinColumn({ name: "article" })
    article: Article;

    @CreateDateColumn({ name: 'created_at' })
    createdAt!: Date;

    @UpdateDateColumn({ name: 'updated_at' })
    updatedDate!: Date;
}
