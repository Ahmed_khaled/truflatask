import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { CrudRequest } from '@nestjsx/crud';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Article } from './entities/article.entity';

@Injectable()
export class ArticlesService extends TypeOrmCrudService<Article>{
  constructor(@Inject('ARTICLE_REPOSITORY') repo) {
    super(repo)
  }

  get base(): TypeOrmCrudService<Article> {
    return this;
  }

  async incrementThumpsUp(req: CrudRequest) {
    let article = await this.base.getOne(req);
    article.thumps_up = article.thumps_up + 1;
    article = await this.base.updateOne(req, article);
    return article;
  }
}
