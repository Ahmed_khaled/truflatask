import { ApiProperty } from "@nestjsx/crud/lib/crud";
import { IsNotEmpty } from "class-validator";
import { Article } from "../../articles/entities/article.entity";

export class CreateCommentDto {

    @ApiProperty()
    @IsNotEmpty({ message: 'please enter content' })
    content: string;

    @ApiProperty({ type: 'number' })
    @IsNotEmpty({
        message: 'please enter article id'
    })
    article: Article;
}
